"""
Band the scores into groups of 50
"""

def scoreBand(score):

    if not isinstance(score, int):
        raise AttributeError('scoreBand() expects int as argument.')

    if 0 <= score <= 50:
        band = '0-50'
    elif 51 <= score <= 100:
        band = '51-100'
    elif 101 <= score <= 150:
        band = '101-150'
    elif 151 <= score <= 200:
        band = '151-200'
    elif 201 <= score <= 250:
        band = '201-250'
    elif 251 <= score <= 300:
        band = '251-300'
    elif 301 <= score <= 350:
        band = '301-350'
    elif 351 <= score <= 400:
        band = '351-400'
    elif 401 <= score <= 450:
        band = '401-450'
    elif 451 <= score <= 500:
        band = '451-500'
    elif 501 <= score <= 550:
        band = '501-550'
    elif 551 <= score <= 600:
        band = '551-600'
    elif 601 <= score <= 650:
        band = '601-650'
    elif 651 <= score <= 700:
        band = '651-700'
    elif 701 <= score: 
        band = '700+'

    return band