"""
   Will scan through the ./Source/Report/ directory & sub-directories
   Will read in each report json object, parse it to the 'TargetReportAll' target schema
   Once all objects have been read and parsed, the in-memory data will be written as a 
   JSON object to the target folder.

   -------------------------------------
   v1:  Rod Pell.     16-Oct-2021
"""

## ... Yes ... this code is essentially a clone of the other '...ToTarget' scripts.
## Am sure there's dozens of ways to combine and avoid duplicate logic 
## but I'll leave this as is.

import json
import os
from fnmatch import fnmatch
from pathlib import Path
from KeysExist import keysExist

targetReportAllList = []

sourcePath = os.getcwd()+'/Source/reports'
pattern = "*.json"

# scroll through all dir & sub-dirs, reading in all .json files found:
for path, subdirs, files in os.walk(sourcePath):
    for name in files:
        if fnmatch(name, pattern):
            # Open each .json file and parse the contents:
            with open(path+'/'+name) as input_file:
                fullJson = json.load(input_file)

                # Empty dictionary to build up with only the data we're interested in.
                reducedReport = {}

                # Mandatory fields
                reducedReport['uuid'] = fullJson['user-uuid']
                reducedReport['pulledTimestamp'] = fullJson['pulled-timestamp']
                
                # Optional fields - insert default value if missing.
                if keysExist(fullJson, 'report','ScoreBlock','Delphi'):
                    reducedReport['score'] = int(fullJson['report']['ScoreBlock']['Delphi'][0]['Score'])
                else:
                    reducedReport['score'] = 0
                
                # Add to master list:
                targetReportAllList.append(reducedReport)

# Write TargetReportSummary out as a JSON file in the target dir:
targetFile = os.getcwd()+'/Target/TargetReportAll.json'
targetFilePath = Path(targetFile)

if targetFilePath.is_file():
    os.remove(targetFilePath)

with open(targetFile, 'w') as output_file:
    json.dump(targetReportAllList, output_file)