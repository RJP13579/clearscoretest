"""
   Will read in the target JSON objects as Data Frames and run the analysis
   Analysis output as 4 .csv files in the OutputCsv dir

   -------------------------------------
   v1:  Rod Pell.     16-Oct-2021
"""

import json
import os
import pandas as pd

# Ingest all Target JSON objects as Data Frames:
targetFileList = ['/Target/TargetAccount.json','/Target/TargetReportAll.json','/Target/TargetReportLatest.json']

for target in targetFileList:
    targetPath = os.getcwd()+target
    targetFile = open(targetPath, 'r')
    targetData = json.load(targetFile)
    targetFile.close()

    if target == '/Target/TargetAccount.json':
        account_df = pd.json_normalize(targetData)
    elif target == '/Target/TargetReportAll.json':
        reportAll_df = pd.json_normalize(targetData)
    elif target == '/Target/TargetReportLatest.json':
        reporLatest_df = pd.json_normalize(targetData)

# Clean up
del targetFileList, targetPath, targetFile, targetData

#  - - - - - - - - - - - 
#     Req1:  Avg credit score across ALL reports
#  - - - - - - - - - - - 
req1Ans = reportAll_df['score'].mean()

#  - - - - - - - - - - - 
#     Req2:  Num of users by employment status
#  - - - - - - - - - - - 
req2Ans = account_df.groupby(['employmentStatus']).size()

#  - - - - - - - - - - - 
#     Req3:  Users per score range
#  - - - - - - - - - - - 
req3Ans = reporLatest_df.groupby(['scoreBand']).size()

#  - - - - - - - - - - - 
#     Req4:  Summary report joining account to latest report:
#  - - - - - - - - - - - 
req4Ans = pd.merge(reporLatest_df, account_df, on='uuid', how='outer')
req4Ans.drop(['pulledTimestamp','score','scoreBand'],axis=1, inplace=True)


#
#  Now print the answers as CSV files to ./OutputCsv
#
reportsPath = os.getcwd()+'/OutputCsv/req1Ans.csv'
req1AnsFile = open(reportsPath, 'w')
req1AnsFile.write(str(req1Ans))
req1AnsFile.close()

reportPath = os.getcwd()+'/OutputCsv/req2Ans.csv'
req2Ans.to_csv(reportPath)

reportPath = os.getcwd()+'/OutputCsv/req3Ans.csv'
req3Ans.to_csv(reportPath)

reportPath = os.getcwd()+'/OutputCsv/req4Ans.csv'
req4Ans.to_csv(reportPath, index=False)

