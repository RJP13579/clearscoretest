# ClearScoreTest

A test test as part of my interview process with ClearScore

## A) Directory and File Setup:

- [ ] Create a base directory to run all code from. (In this example, it’s called ‘RodPell’)
- [ ] Create 3 directories in the base directory, called:
``` 
    Source  
    Target  
    OutputCsv
``` 
- [ ] Copy the source data (Account and Reports dirs) into the Source directory.
- [ ] Copy the 6 python programs into the base directory. 
``` 
    1-AccountToTarget.py
    2-ReportAllToTarget.py
    3-ReportLatestToTarget.py
    4-ProduceReports.py
    KeysExist.py
    ScoreBand.py
``` 

Should look something like this:  
![Should look something like this](https://rodpell-clearscoretest.s3.eu-west-1.amazonaws.com/ClearScoreImage.png)

## B) Run the python programs - in the correct order
- [ ]  1-AccountToTarget.py  
- [ ]  2-ReportAllToTarget.py  
- [ ]  3-ReportLatestToTarget.py  
- [ ]  4-ProduceReports.py  
(The other two programs are imported functions, and do not need to be explicitly called)

## Inspect outputs:
The final .CSVs as per the requirements will be found in ./OutputCsv  
./Target holds the intermediate ‘target’ state of the source JSON objects, as described in the ‘Analysis’ doc.
