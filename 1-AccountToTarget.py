"""
   Will scan through the ./Source/Account/ directory
   Will read in each report json object, parse it to the 'TargetAccount' target schema
   Once all objects have been read and parsed, the in-memory data will be written as a 
   JSON object to the target folder.

   -------------------------------------
   v1:  Rod Pell.     16-Oct-2021
"""

## ... Yes ... this code is essentially a clone of the other '...ToTarget' scripts.
## Am sure there's dozens of ways to combine and avoid duplicate logic 
## but I'll leave this as is.

import json
import os
from fnmatch import fnmatch
from pathlib import Path
from KeysExist import keysExist

targetAccountList = []

path = os.getcwd()+'/Source/accounts'
pattern = "*.json"

# scroll through all dir & sub-dirs, reading in all .json files found:
for path, subdirs, files in os.walk(path):
    for name in files:
        if fnmatch(name, pattern):
            # Open each .json file and parse the contents:
            with open(path+'/'+name) as input_file:
                fullJson = json.load(input_file)

                # Empty dictionary to build up with only the data we're interested in.
                reducedAccount = {}

                # Mandatory fields:
                reducedAccount['uuid'] = fullJson['uuid']
                
                # Optional fields - insert default value if missing.
                if keysExist(fullJson, 'account','user','bankName'):
                    reducedAccount['bankName'] = fullJson['account']['user']['bankName']
                else:
                    reducedAccount['bankName'] = 'Data missing at source'
                if keysExist(fullJson, 'account','user','employmentStatus'):
                    reducedAccount['employmentStatus'] = fullJson['account']['user']['employmentStatus']
                else:
                    reducedAccount['employmentStatus'] = 'Data missing at source'
                
                # Add to master list:
                targetAccountList.append(reducedAccount)

# Write targetAccountList out as a JSON file in the target dir:
targetFile = os.getcwd()+'/Target/TargetAccount.json'
targetFilePath = Path(targetFile)

if targetFilePath.is_file():
    os.remove(targetFilePath)

with open(targetFile, 'w') as output_file:
    json.dump(targetAccountList, output_file)