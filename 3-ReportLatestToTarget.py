"""
   Will scan through the ./Source/Report/ directory & sub-directories
   Will read in each report json object, parse it to the 'TargetReportLatest' target schema

   Will also read the previously created '/Target/TargetReportAll.json' object to 
   determine the max(Timestamp) per user id
   Any source reports without the max(Timestamp) per uuid will be rejected.

   Once all objects have been read and parsed, the in-memory data will be written as a 
   JSON object to the target folder.

   -------------------------------------
   v1:  Rod Pell.     16-Oct-2021
"""

## ... Yes ... this code is essentially a clone of the other '...ToTarget' scripts.
## Am sure there's dozens of ways to combine and avoid duplicate logic 
## but I'll leave this as is.
## This one does have unique functionality to filter out non-current reports

import json
import os
import pandas as pd
from fnmatch import fnmatch
from pathlib import Path
from KeysExist import keysExist
from ScoreBand import scoreBand

# Read TargetReportSummary.json as a reference for the max timestamp:
allTSPath = os.getcwd()+'/Target/TargetReportAll.json'
allTSFile = open(allTSPath, 'r')
allTSData = json.load(allTSFile)
allTSFile.close()

# Create a dataframe, group by the uuid and consolidate to only the maxTimestamp per uuid
allTS_df = pd.json_normalize(allTSData)
allTS_df.drop('score',axis=1, inplace=True)
allTSgroup_df = allTS_df.groupby('uuid')
maxTS_df = allTSgroup_df.max().reset_index()

# Clean up
del allTSPath, allTSFile, allTSData, allTS_df, allTSgroup_df

#  Now basically the same code as before ... but extra step comparing to MaxTS_df as a filter.

targetReportLatestList = []

sourcePath = os.getcwd()+'/Source/reports'
pattern = "*.json"

# scroll through all dir & sub-dirs, reading in all .json files found:
for path, subdirs, files in os.walk(sourcePath):
    for name in files:
        if fnmatch(name, pattern):
            # Open each .json file and parse the contents:
            with open(path+'/'+name) as input_file:
                fullJson = json.load(input_file)
                
                # Compare the timesamp to the reference MaxTS.  If not equal, skip to next iteration
                ref_df = maxTS_df.loc[maxTS_df['uuid'] == fullJson['user-uuid']].reset_index()
                if fullJson['pulled-timestamp'] == ref_df.iloc[0,2]: 
                    # Empty dictionary to build up with only the data we're interested in.
                    reducedReport = {}
                    
                    # Mandatory fields
                    reducedReport['uuid'] = fullJson['user-uuid']
                    reducedReport['pulledTimestamp'] = fullJson['pulled-timestamp']
                    
                    # Optional fields - insert default value if missing.
                    if keysExist(fullJson, 'report','ScoreBlock','Delphi'):
                        reducedReport['score'] = int(fullJson['report']['ScoreBlock']['Delphi'][0]['Score'])
                    else:
                        reducedReport['score'] = 0
                    # Band the score:
                    reducedReport['scoreBand'] = scoreBand(reducedReport['score'])
                    
                    if keysExist(fullJson, 'report','Summary','Payment_Profiles','CPA','Bank',\
                                  'Total_number_of_Bank_Active_accounts_'):
                        reducedReport['totalActiveBankAccts'] = \
                                    int(fullJson['report']['Summary']['Payment_Profiles']['CPA']['Bank']\
                                    ['Total_number_of_Bank_Active_accounts_'])
                    else:
                        reducedReport['totalActiveBankAccts'] = 0
                    
                    if keysExist(fullJson, 'report','Summary','Payment_Profiles','CPA','Bank',\
                                  'Total_outstanding_balance_on_Bank_active_accounts'):
                        reducedReport['totalOustandingBalance'] = \
                                    int(fullJson['report']['Summary']['Payment_Profiles']['CPA']['Bank']\
                                    ['Total_outstanding_balance_on_Bank_active_accounts'])
                    else:
                        reducedReport['totalOustandingBalance'] = 0
                    
                    # Add to master list:
                    targetReportLatestList.append(reducedReport)
                    
# Write TargetReportLatest out as a JSON file in the target dir:
targetFile = os.getcwd()+'/Target/TargetReportLatest.json'
targetFilePath = Path(targetFile)

if targetFilePath.is_file():
    os.remove(targetFilePath)

with open(targetFile, 'w') as output_file:
    json.dump(targetReportLatestList, output_file)