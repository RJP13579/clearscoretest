"""
Check if nested keys exist in a python dict
"""

def keysExist(element, *keys):
    if not isinstance(element, dict):
        raise AttributeError('keysExists() expects dict as first argument.')
    if len(keys) == 0:
        raise AttributeError('keysExists() expects at least two arguments, one given.')

    _element = element
    for key in keys:
        try:
            _element = _element[key]
        except KeyError:
            return False
    return True